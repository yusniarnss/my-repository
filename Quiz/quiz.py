import shapefile                                                                
w=shapefile.Writer()                                                            
w.shapeType                                                                     

w.field("kolom1","C")                                                           
w.field("kolom2","C")

w.record("segitiga","bertua")                                                   
w.record("segitiga","beradik")
w.record("segitiga","berkaka")
w.record("segitiga","berpapah")
w.record("segitiga","bermamah")
w.record("segitiga","berduda")
w.record("segitiga","berjanda")
w.record("segitiga","beranak")
w.record("segitiga","berkeluarga")

w.poly(parts=[[[-1,1],[0,3],[1,1],[-1,1]]],shapeType=shapefile.POLYLINE)        
w.poly(parts=[[[-1,5],[0,3],[1,5],[-1,5]]],shapeType=shapefile.POLYLINE)
w.poly(parts=[[[2,2],[0,3],[2,4],[2,2]]],shapeType=shapefile.POLYLINE)
w.poly(parts=[[[-2,2],[0,3],[-2,4],[-2,2]]],shapeType=shapefile.POLYLINE)
w.poly(parts=[[[-2,2],[-4,3],[-2,4],[-2,2]]],shapeType=shapefile.POLYLINE)
w.poly(parts=[[[-3,1],[-4,3],[-5,1],[-3,1]]],shapeType=shapefile.POLYLINE)
w.poly(parts=[[[-6,2],[-4,3],[-6,4],[-6,2]]],shapeType=shapefile.POLYLINE)
w.poly(parts=[[[-3,5],[-4,3],[-5,5],[-3,5]]],shapeType=shapefile.POLYLINE)
w.poly(parts=[[[-3,5],[-4,6],[-5,5],[-3,5]]],shapeType=shapefile.POLYLINE)

w.save("quiz")                                                                